import React, { Component } from 'react'
import './App.css';
import Search from './component/Search'
import News from './component/News'
import Register from './component/Register'

export class App extends Component {
  render() {
    return (
      <div className="newsfeed">
        <header className="header">
          <h1 className="header-ttl"><a href="/">newsfeed</a></h1>
          <p className="header-sub">All your top breaking news stories and headlines from around the world updated regularly from many sources different.</p>
        </header>
        <main className="content">
          <div className="articles">
            <Search />
            <News />
          </div>
          <Register />
        </main>
        <footer className="footer">
          <p className="footer-src">src: <a href="https://newsapi.org/">newsapi</a></p>
          <p className="footer-copy">© {(new Date().getFullYear())} testing reactjs</p></footer>
      </div>
    )
  }
}

export default App
