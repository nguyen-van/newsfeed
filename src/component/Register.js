import React, { Component } from 'react';

export default class Register extends Component {
  userData;
  constructor(props) {
    super(props);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {
      name: '',
      email: '',
    }
  }
  onChangeName(e) {
    this.setState({ name: e.target.value })
  }
  onChangeEmail(e) {
    this.setState({ email: e.target.value })
  }
  componentDidMount() {
    this.userData = JSON.parse(localStorage.getItem('user'));
    this.passData = JSON.parse(localStorage.getItem('pass'));
    if (localStorage.getItem('user')) {
      this.setState({
        name: this.userData.name,
        email: this.userData.email
      })
    } else {
      this.setState({
        name: '',
        email: '',
      })
    }
  }
  componentWillUpdate(nextState) {
    localStorage.setItem('user', JSON.stringify(nextState));
  }
  onSubmit(e) {
    e.preventDefault()
    console.log(this.state.props)
  }
  render() {
    return (
      <div className="sidebar">
        <form onSubmit={this.onSubmit} className="form">
          <div className="form-group">
            <label className="form-label">Name</label>
            <input type="text" className="form-control" value={this.state.name} onChange={this.onChangeName} />
          </div>
          <div className="form-group">
            <label className="form-label">Email</label>
            <input type="email" className="form-control" value={this.state.email} onChange={this.onChangeEmail} />
          </div>
          <button type="submit" className="btn">Register</button>
        </form>
      </div>
    )
  }
}
