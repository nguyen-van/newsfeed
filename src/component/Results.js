import React from 'react'
import Item from './Item'

function Results({ results }) {
  return (
    <section className="results">
      {results.map((element, init) => (
        <Item key={init.description} item={element} />
      ))}
    </section>
  )
}

export default Results
