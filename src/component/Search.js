import React from 'react';
import axios from 'axios';

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      query: '',
      results: {},
      loading: false,
      message: '',
    };
    this.cancel = '';
  }
  fetchSearchResults = (updatedPageNo = '', query) => {
    const pageNumber = updatedPageNo ? `&page=${updatedPageNo}` : '';
    const searchUrl = `https://newsapi.org/v2/top-headlines?country=us&apiKey=1a9ac94d23274b7f9624cd5ad4840597&q=${query}${pageNumber}`;
    if (this.cancel) {
      this.cancel.cancel();
    }
    this.cancel = axios.CancelToken.source();
    axios.get(searchUrl, {
      cancelToken: this.cancel.token
    })
      .then(res => {
        const resultNotFoundMsg = !res.data.articles.length
          ? 'Please try again'
          : '';
        this.setState({
          results: res.data.articles,
          message: resultNotFoundMsg,
          loading: false
        })
      })
      .catch(error => {
        if (axios.isCancel(error) || error) {
          this.setState({
            loading: false,
            message: 'Please try again'
          })
        }
      })
  };
  handleOnInputChange = (event) => {
    const query = event.target.value;
    if (!query) {
      this.setState({ query, results: {}, message: '', totalPages: 0, totalResults: 0 });
    } else {
      this.setState({ query, loading: true, message: '' }, () => {
        this.fetchSearchResults(1, query);
      });
    }
  };
  renderSearchResults = () => {
    const { results } = this.state;
    if (Object.keys(results).length && results.length) {
      return (
        <ul className="list">
          {results.map((result) => {
            return (
              <li className="list-item" key={result}>
                <a href={result.url} target="_blank">
                  <div className="list-img"><img src={result.urlToImage} /></div>
                  <div className="list-cnt">
                    <p className="list-ttl">{result.title}</p>
                    <p className="list-date">{result.publishedAt}</p>
                  </div>
                </a>
              </li>
            );
          })}
        </ul>
      )
    }
  };
  render() {
    const { query, message } = this.state;
    return (
      <div className="articles-inner">
        <section className="searchbox-wrap" htmlFor="search-input">
          <input
            type="text"
            name="query"
            value={query}
            id="search-input"
            placeholder="Search something (apple,...)"
            className="searchbox"
            onChange={this.handleOnInputChange}
          />
          {message && <p className="message">{message}</p>}
        </section>
        {this.renderSearchResults()}
      </div>
    )
  }
}

export default Search
