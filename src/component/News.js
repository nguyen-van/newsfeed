import React, { Component } from 'react';

class News extends Component {
  constructor(props) {
    super(props);
    this.state = {
      articles: null,
    }
  }
  componentDidMount() {
    fetch('https://newsapi.org/v2/top-headlines?country=us&apiKey=1a9ac94d23274b7f9624cd5ad4840597')
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        console.log(data);
        this.setState({
          articles: data.articles
        })
      })

  }
  render() {
    // console.log(this.state);
    return (
      <div className="news">
        <p className="news-ttl">News US</p>
        <ul className="list">
          {(this.state.articles || []).map((element, index) => {
            return (
              <li className="list-item" key={index}>
                <a href={element.url} target="_blank">
                  <div className="list-img"><img src={element.urlToImage} /></div>
                  <div className="list-cnt">
                    <p className="list-ttl">{element.title}</p>
                    <p className="list-date">{element.publishedAt}</p>
                  </div>
                </a>
              </li>
            )
          })}
        </ul>
      </div>
    );
  }
}

export default News;
