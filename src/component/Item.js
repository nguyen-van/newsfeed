import React from 'react'

function Item({ item }) {
  return (
    <div className="inner">
      <div className="list-img"><img src={item.urlToImage} /></div>
      <div className="list-cnt">
        <p className="list-ttl">{item.title}</p>
        <p className="list-date">{item.publishedAt}</p>
      </div>
    </div>
  )
}

export default Item
